#include <ncurses.h>
#include <string.h>

void init_colors();
void center(int row, int color, char *message);

int main() {
    int y, width;

    initscr();
    init_colors();

    center(1, 1, "All the shine of a thousand spotlights");
    center(3, 2, "All the stars we steal from the night sky");
    center(5, 3, "Will never be enough");
    center(7, 4, "Never be enough");
    center(9, 5, "Towers of gold are still too little");
    center(11, 6, "These hands could hold the world but it'll");
    center(13, 7, "Never be enough");

    getch();

    endwin();
    return 0;
}

void init_colors() {
    start_color();

    init_pair(1, COLOR_BLACK, COLOR_RED);
    init_pair(2, COLOR_BLACK, COLOR_GREEN);
    init_pair(3, COLOR_BLACK, COLOR_YELLOW);
    init_pair(4, COLOR_BLACK, COLOR_BLUE);
    init_pair(5, COLOR_BLACK, COLOR_MAGENTA);
    init_pair(6, COLOR_BLACK, COLOR_CYAN);
    init_pair(7, COLOR_BLACK, COLOR_WHITE);
}

void center(int row, int color, char *message) {
    int len,indent,y,width;

    /* get screen width */
    getmaxyx(stdscr, y, width);

    /* get title length */
    len = strlen(message);

    /* calculate indent */
    indent = (width - len)/2;

    /* set attributes */
    attrset(COLOR_PAIR(color) | A_BOLD | A_REVERSE);

    /* show the string */
    mvaddstr(row, indent, message);

    refresh();
}
