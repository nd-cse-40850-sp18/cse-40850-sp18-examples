#include <ncurses.h>
   
int main() {
    char *t1 = "Hello, World!";
    char *t2 = "Hola, Mundo!";

    initscr();		    /* Initialize ncurses */

    box(stdscr,'*','*');    /* Draw box */

    for (char *s = t1; *s; s++) {
    	addch(*s);	    /* Output one character */
	refresh();	    /* Update the screen */
	napms(100);	    /* Delay .1 seconds */
    }
    getch();		    /* Wait for input */

    move(2, 0);		    /* Move to row 3, column 1 */

    addstr(t2);		    /* Output string */
    refresh();		    /* Update the screen */
    getch();		    /* Wait for input */

    endwin();		    /* Close ncurses */
    return 0;
}
