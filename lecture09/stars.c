#include <ncurses.h>
#include <stdlib.h>

void init_colors();

int main() {
    initscr();
    curs_set(0);

    init_colors();

    int height, width;
    getmaxyx(stdscr, height, width);

    for (int i = 0; i < 1000; i++) {
	attrset(COLOR_PAIR(rand() % 8) | A_BOLD);
	mvaddch(rand() % height, rand() % width, '*');
	refresh();
    	napms(100);
    }

    getch();
    endwin();
    return 0;
}

void init_colors() {
    start_color();

    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_YELLOW, COLOR_BLACK);
    init_pair(4, COLOR_BLUE, COLOR_BLACK);
    init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(6, COLOR_CYAN, COLOR_BLACK);
    init_pair(7, COLOR_WHITE, COLOR_BLACK);
}
